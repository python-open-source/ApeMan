"""
Restructured
============

.. note ::

   These tests are meant to ensure that an overlay may be leverage to restrcture a given scaffold.

.. todo ::

   Presently ApeMan provides no means of restructuring a given scaffold.
   To do so would require it to accept a mapping from the scaffold to the restructured patches.
   
"""
# class testRestructure(unittest.TestCase) :
#  """
#  This tests the import mechanism with an overlay that restructures everything
#  """
# 
# # Merge the following comment into the class below
# #
# # class testPackageStructure(unittest.TestCase) :
# #  # This essentially tests the name of an imported module matches the target name
# #  # This is not necessarily a 1 to 1 relation e.g. overlay.tiers might map to tiers or _tiers_ 
# #  def setUp(self) :
# #   pass
# # 
# #  def tearDown(self) : 
# #   pass
# # 
# #  def testTiers(self):  
# #   """Asserts Class(es) Existence in Overlay"""
# #   from uppercase import tiers as module # Do this with masks, direct imports fail if another script calls this one and relative imports fails if this is called directly
# #   self.assertEqual(module.__name__,    'uppercase.tiers')  # Ideally : _tiers_
# #   self.assertEqual(module.__version__, '1.0', __doc__)
# # #   print(module.__name__)
# # #   self.assertEqual(module.__version__, '1.1', __doc__)
# # 
#
# class testPackageStructure(unittest.TestCase) : # Originally : TestClasses, Rename to testStructure
#  # The following tests are a bit redundant but are intended to
#  # test module renaming occurs correctly. Name Spaced packages
#  # would fail against these tests as they do not for __init__ 
#  # files.
# 
#  mask = {
#   "import":
#    "\n".join([
#     "import json",
#     "from {} import {} as module",
#     "print(json.dumps({{'name':module.__name__,'version':module.__version__}}))"
#    ]),
#  }
# 
#  def setUp(self) :
#   pass
# 
#  def tearDown(self) : 
#   pass
# 
#  def testTiers(self):
#   """Asserts Class(es) Existence in Overlay"""
#   result = osexec(self.mask["import"], "uppercase","tiers")
#   answer = 'uppercase.tiers'                                  # Ideally : _tiers_
#   self.assertEqual(answer, result["name"], "Ensure module name is correctly renamed")
#   answer = '1.0'
#   self.assertEqual(answer, result["version"], "Ensure version text is overwritten")
# 
#  def testTierA(self):
#   """Asserts Class(es) Existence in Overlay"""
#   result = osexec(self.mask["import"], "uppercase.tiers","package_a")
#   answer = 'uppercase.tiers.package_a'                        # Ideally : _tiers_
#   self.assertEqual(answer, result["name"], "Ensure module name is correctly renamed")
#   answer = '1.1'
#   self.assertEqual(answer, result["version"], "Ensure version text is overwritten")
# 
#  def testTierB(self):
#   """Asserts Class(es) Existence in Overlay"""
#   result = osexec(self.mask["import"], "uppercase.tiers.package_a","package_b")
#   answer = 'uppercase.tiers.package_a.package_b'              # Ideally : _tiers_._package_a_._package_b_
#   self.assertEqual(answer, result["name"], "Ensure module name is correctly renamed")
#   answer = '1.2'
#   self.assertEqual(answer, result["version"], "Ensure version text is overwritten")
# 
#  def testTierC(self):
#   """Asserts Class(es) Existence in Overlay"""
#   result = osexec(self.mask["import"], "uppercase.tiers.package_a.package_b","package_c")
#   answer = 'uppercase.tiers.package_a.package_b.package_c'    # Ideally : _tiers_
#   self.assertEqual(answer, result["name"], "Ensure module name is correctly renamed")
#   answer = '1.3'
#   self.assertEqual(answer, result["version"], "Ensure version text is overwritten")
#

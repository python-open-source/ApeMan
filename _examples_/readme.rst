Module
======

The `module` and `package` example fails when python is called upon it directly 
::

python module/package

butsucceeds when called as module
::

python -m module/package
